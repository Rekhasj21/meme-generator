import React from "react";

export default function MemeCard(props) {
    const { url, topText, bottomText } = props

    return (
        <div className="meme-card">
            <img src={url} className='meme-image' />
            <h1 className="meme-text top">{topText}</h1>
            <h1 className="meme-text bottom">{bottomText}</h1>
        </div>
    )
}