import React from "react"
import MemeCard from "./MemeCard"

export default function Input() {
    const [memeUrl, setMemeUrl] = React.useState({
        topText: "",
        bottomText: "",
        randomImage: "https://i.imgflip.com/wxica.jpg"
    })
    const [allMemes, setAllMemesData] = React.useState([])

    React.useEffect(() => {
        async function getData() {
            const response = await fetch("https://api.imgflip.com/get_memes")
            const memeData = await response.json()
            setAllMemesData(memeData.data.memes)
        }
        getData()
    }, [])

    function getMemes() {
        const randomNumber = Math.floor(Math.random() * allMemes.length)
        const newUrl = allMemes[randomNumber].url

        setMemeUrl(prevUrl => ({
            ...prevUrl,
            randomImage: newUrl
        }))
    }
    function handleChange(event) {
        const { value, name } = event.target
        setMemeUrl(prevUrl => ({
            ...prevUrl,
            [name]: value
        }))
    }
    return (
        <div>
            <div className="form-container" >
                <input type="text" placeholder="enter top text" name='topText' value={memeUrl.topText} onChange={handleChange} className="user-input" />
                <input type="text" placeholder="enter bottom text" name='bottomText' value={memeUrl.bottomText} onChange={handleChange} className="user-input" />
                <button className="button-text" onClick={getMemes}>Get a new meme image  🖼 </button>
            </div>
            <MemeCard url={memeUrl.randomImage} topText={memeUrl.topText} bottomText={memeUrl.bottomText} />
        </div>
    )
}