import React from "react"

export default function Navbar(){

    return (
        <nav>
            <img src="/images/troll-face.png" className="troll-face" />
            <h2 className="meme-heading" >Meme Generator</h2>
            <h3 className="project-titile">React Course - Project 3</h3>
        </nav>  
    )
}