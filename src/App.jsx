import React from 'react'
import Navbar from './assets/Navbar'
import Input from './assets/Input'
import "/src/App.css"

function App() {
  return (
    <div className='app-container'>
      <Navbar />
      <Input />
    </div>
  )
}

export default App
